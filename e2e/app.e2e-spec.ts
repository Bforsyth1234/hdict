import { HdictPage } from './app.po';

describe('hdict App', () => {
  let page: HdictPage;

  beforeEach(() => {
    page = new HdictPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
