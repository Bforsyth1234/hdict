// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBvAaXeVuPyCkD9XQ_P-JdePBBf2Ru5k6c',
    authDomain: 'hdict-ae223.firebaseapp.com',
    databaseURL: 'https://hdict-ae223.firebaseio.com',
    projectId: 'hdict-ae223',
    storageBucket: 'hdict-ae223.appspot.com',
    messagingSenderId: '788276961183'
  }
};
