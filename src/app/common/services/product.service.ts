import { Product } from '../models/product.model';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ProductService {
  products: Array<any> = [];
  filteredProducts: Array<any> = [];
  items: FirebaseListObservable<any[]>;

  constructor(
    db: AngularFireDatabase,
    ) {
      this.items = db.list('/products');
  }

  getProducts() {
    this.items.subscribe( item => {
      this.products = item;
    });
  }

  getProductsInCategory(category) {
    this.filteredProducts = [];
    this.items.subscribe( item => {
      this.products = item;
      this.filterProductsByCat(category);
    });
    return this.filteredProducts;
  }

  filterProductsByCat(category) {
    category = category.replace('/home/', '');
    for (let i = 0; i < this.products.length; i++) {
      if (category.toUpperCase() === this.products[i].category.toUpperCase()) {
        this.filteredProducts.push(this.products[i]);
      }
    }
    return this.filteredProducts;
  }

  getSteps(url): Observable<any> {
    return this.getStepsFromFB(url);
  }

  getStepsFromFB(url: string): Observable<any> {
    this.items.subscribe( item => {
      this.filteredProducts = item;
    });
    return this.items;
  }
}
