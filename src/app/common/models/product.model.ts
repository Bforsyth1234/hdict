export interface Product {
  url: string;
  displayName: string;
}
