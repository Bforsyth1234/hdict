import { Component, OnInit } from '@angular/core';
// import { Category } from '../common/models/category.model';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public categories: Array<any> = [
    {url: 'music', displayName: 'Music'},
    {url: 'movies', displayName: 'Movies'},
    {url: 'shopping', displayName: 'Shopping'},
    {url: 'storage', displayName: 'Storage'}
  ];

  constructor() { }

  ngOnInit() {
    console.log('test home');
  }

}
