import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { ProductComponent } from '../product/product.component';
import { StepsComponent } from '../steps/steps.component';


  const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent },
  {path: 'home/music', component: ProductComponent },
  {path: 'home/movies', component: ProductComponent },
  {path: 'home/storage', component: ProductComponent },
  {path: 'home/shopping', component: ProductComponent },
  {path: 'home/music/:productUrl', component: StepsComponent },
  {path: 'home/movies/:productUrl', component: StepsComponent },
  {path: 'home/storage/:productUrl', component: StepsComponent },
  {path: 'home/shopping/:productUrl', component: StepsComponent },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule {
}
