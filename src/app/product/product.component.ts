import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Router } from '@angular/router';


import { Product } from '../common/models/product.model';
import {ProductService} from '../common/services/product.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products;
  url = this.router.url;

  constructor(public productService: ProductService,
              public router: Router
            ) { }

  ngOnInit() {
    this.getProductsPerCategory();
  }

  getProductsPerCategory() {
    this.products = this.productService.getProductsInCategory(this.url);
  }

  navigateToSteps(url) {
    this.router.navigateByUrl(this.url + '/' + url);
  }

}
