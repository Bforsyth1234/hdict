import {ProductService} from '../common/services/product.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css']
})
export class StepsComponent implements OnInit {
    private url = this.router.url;
    public product;

  constructor(public productService: ProductService,
              public router: Router
            ) { }

  ngOnInit() {
    this.getSteps();
  }

  getSteps() {
    this.productService.getSteps(this.url).subscribe(res => {
      this.product = res;
      const formatUrl = this.getProductNameFromUrl(this.url);
      for (let i = 0; i < this.product.length; i++) {
        const filteredUrl = this.product[i].category + '/' + this.product[i].urlName;
        if ( formatUrl === filteredUrl ) {
          this.product = this.product[i];
        }
      }
    });
  }

  getProductNameFromUrl(url: string) {
    return url.replace('/home/', '');
  }

}
